<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>home</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="css/table.css" rel="stylesheet">
</head>

<body>
    <!-- ヘッダー -->
    <header>

        <nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <a class="nav-link" href="HomeServlet">ゲームECサイト</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">

                <li class="nav-item">
                    <a class="nav-link" href="UserDetailServlet?id=${userInfo.id}">マイページ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="CartServlet">カート</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="BookmarkServlet">お気に入り</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
                </li>
            </ul>
        </nav>

    </header>


    <div class="container mt-5">
        <div class="col-3 mx-auto">
            <h5>購入が完了しました</h5>
        </div>
        <div>
            <div class="col-6 mx-auto row">
                <div class="col-6 center-align">
                    <a href="HomeServlet" class="btn waves-effect waves-light ">引き続き買い物をする</a>
                </div>
                <div class="col-6 center-align">
                    <a href="UserDetailServlet?id=${userInfo.id}" class="btn waves-effect waves-light">マイページへ</a>
                </div>
            </div>
        </div>
        <div class="col-2 mx-auto">
            <h5>購入詳細</h5>
        </div>

        <div class="col-8 mx-auto">

            <div>
                <div>
                    <table>
                        <thead>
                            <tr>
                                <th>購入日時</th>
                                <th>決済方法</th>
                                <th>合計金額</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>${buyInfo.buyDate}</td>
                                <td>${buyInfo.buyMethod}</td>
                                <td>${buyInfo.totalPrice}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- 詳細 -->
        <div class="col-6 mx-auto">
            <table>
                <thead>
                    <tr>
                        <th width="300">商品名</th>
                        <th width="200">価格</th>
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <td>ゲーム1</td>
                        <td>2,000円</td>
                    </tr>

                    <tr>
                        <td>ゲーム2</td>

                        <td>2,000円</td>
                </tbody>
            </table>
        </div>

    </div>

</body>

</html>