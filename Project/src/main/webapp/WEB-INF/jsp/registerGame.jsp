<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>商品登録</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <a class="nav-link" href="home.html">ゲームECサイト</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <li class="nav-item">
                    <a class="nav-link" href="UserDetailServlet?id=${userInfo.id}">マイページ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="CartServlet">カート</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="bookmark.html">お気に入り</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="login.html">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>




    <div class="container mt-5">
        <div class="col-3 mx-auto">
            <h3>商品新規登録</h3>
        </div>
        <!--   登録フォーム   -->
        <form action="RegisterGameServlet" method="post">
         <input type="hidden" name="user-id" value="${userInfo.id}">
        <div class="col-8 mx-auto ">
            <table>
                <tbody>
                    <tr height="80">
                        <td class="center" width="200">商品名</td>
                        <td class="center"><input type="text" name="game-name"></td>
                    </tr>
                    <tr height="80">
                        <td class="center">アイコン</td>
                        <td class="center"><input type="file" id ="iconfile" name="icon"></td>
                    </tr>
                    <tr height="80">
                        <td class="center">価格</td>
                        <td class="center"><input type="text" name="price">円</td>
                    </tr>
                    <tr height="80">
                        <td class="center">説明文</td>
                        <td class="center"><textarea name="detail" style="width:150%;" placeholder="1000文字以内で入力してください"></textarea></td>
                    </tr>
                    <tr>
                        <td class="center">ジャンル</td>
                        <td class="center">
                            <select name="genre-id" id="genre">
                                <c:forEach var="genreList" items="${genreList}" >
									<option value="${genreList.id}">${genreList.name}</option>
								</c:forEach>
                            </select>
                              
                        </td>

                        

                    </tr>
                </tbody>
            </table>
            <div class="col-3 mx-auto mt-3">
                <button class="btn btn-primary" type="submit">登録</button>
            </div>
        </div>
        </form>
    </div>

</body>

</html>