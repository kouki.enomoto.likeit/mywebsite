<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>login</title>
    <!-- login.cssの読み込み -->
    <link href="css/login.css" rel="stylesheet" type="text/css" />
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <a class="nav-link" href="HomeServlet">ゲームECサイト</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <li class="nav-item">
                    <a class="btn btn-primary" href="UserAddServlet">新規登録</a>
                </li>
            </ul>
        </nav>
    </header>

    <!-- ログインフォーム -->
    
        <div class="text-center mb-4">
            <h1 class="h3 mb-3">ログイン画面</h1>
        </div>
        <div class="row">
			<div class="col-6 offset-3 mb-5">
				<c:if test="${errMsg != null }">
				<div class="alert alert-danger" role="alert">${errMsg}</div>
				</c:if>
			</div>
		</div>
		
		<form class="form-signin mt-5" action="LoginServlet" method="post">
        <div class="row mb-4 col-10 mx-auto">
            <label>ログインID</label>
            <input class="ml-3 form-control" type="text" name="loginId" id="inputLoginId" value="${loginId}">
        </div>
        <div class="row mb-4 col-10 mx-auto">
            <label>パスワード</label>
            <input class="ml-3 form-control" type="password" name="password" id="inputPassword">
        </div>
        <div class="col-4 mx-auto">
            <button class="btn btn-primary" type="submit">ログイン</button>
        </div>
    </form>

</body>

</html>