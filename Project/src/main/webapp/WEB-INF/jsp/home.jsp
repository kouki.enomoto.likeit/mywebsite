<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ゲームECサイト</title>
    <!-- home.cssの読み込み -->
    <link href="css/home.css" rel="stylesheet" type="text/css" />
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <a class="nav-link" href="HomeServlet">ゲームECサイト</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">

                <li class="nav-item">
                    <a class="nav-link" href="UserDetailServlet?id=${userInfo.id}">マイページ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="CartServlet">カート</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="BookmarkServlet">お気に入り</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>

    <!-- 検索欄 -->


    <!-- おすすめ商品欄 -->
    <div class="container mt-5">
        
            <h5>あなたへのおすすめ</h5>
       
        <div class="section">
            <div class="row">

                <c:forEach var="recommendGame" items="${recommendGame}">
                <!--   商品2   -->
                <div class="col s12 m6">
                    <div class="card">
                        <div class="card-image">
                            <a href="GameDetailServlet?game_id=${recommendGame.id}"><img src="img/${recommendGame.icon}"></a>
                        </div>
                        <div class="card-content">
                            <span class="card-title">${recommendGame.name}</span>
                            <p>${recommendGame.price}円
                            </p>
                        </div>
                    </div>
                </div>
                </c:forEach>


            </div>
        </div>
        
    </div>
    
    <div class="search col-5 mx-auto row">
    <form action="GameSearchServlet">
        <div class="col-8">
            <input class="text" name="search_word">
        </div>
        <div class="col-4">
            <button type="submit" class="btn btn-primary">検索</button>
        </div>
        </form>
    </div>


    <div class="container mt-5">
        <div class="">
            <h5>ジャンルで探す</h5>
        </div>

        <div class="section">
            <div class="row">
                <div class="col sm 3">
                    <div class="card">
                        <div class="card-image">
                            <a href="serch.html">アクション<img src="img/genreimage/action.png"></a>
                        </div>
                    </div>

                </div>
                <div class="col sm 3">
                    <div class="card">
                        <div class="card-image">
                            <a href="serch.html">アドベンチャー<img src="img/genreimage/adventure.png"></a>
                        </div>
                    </div>

                </div>

                <div class="col sm 3">
                    <div class="card">
                        <div class="card-image">
                            <a href="serch.html">シューティング<img src="img/genreimage/shooting.png"></a>
                        </div>
                    </div>

                </div>
                <div class="col sm 3">
                    <div class="card">
                        <div class="card-image">
                            <a href="serch.html">シミュレーション<img src="img/genreimage/simulation.png"></a>
                        </div>
                    </div>

                </div>



            </div>
            <div class="row">
                <div class="col sm 3">
                    <div class="card">
                        <div class="card-image">
                            <a href="serch.html">ロールプレイング<img src="img/genreimage/rpg.png"></a>
                        </div>
                    </div>

                </div>
                <div class="col sm 3">
                    <div class="card">
                        <div class="card-image">
                            <a href="serch.html">サンドボックス<img src="img/genreimage/sandbox.png"></a>
                        </div>
                    </div>

                </div>

                <div class="col sm 3">
                    <div class="card">
                        <div class="card-image">
                            <a href="serch.html">レース<img src="img/genreimage/race.png"></a>
                        </div>
                    </div>

                </div>
                <div class="col sm 3">
                    <div class="card">
                        <div class="card-image">
                            <a href="serch.html">パズル<img src="img/genreimage/puzzle.png"></a>
                        </div>
                    </div>

                </div>



            </div>
        </div>

        <div class="container mt-5">
            <div class="">
                <h5>今週の新作</h5>
            </div>

            <div class="section">
                <div class="row">
                    <div class="col sm 3">
                        <div class="card">
                            <div class="card-image">
                                <a href=""><img src="img/gameicon.jpeg"></a>
                            </div>
                        </div>

                    </div>
                    <div class="col sm 3">
                        <div class="card">
                            <div class="card-image">
                                <a href=""><img src="img/gameicon.jpeg"></a>
                            </div>
                        </div>

                    </div>

                    <div class="col sm 3">
                        <div class="card">
                            <div class="card-image">
                                <a href=""><img src="img/gameicon.jpeg"></a>
                            </div>
                        </div>

                    </div>
                    <div class="col sm 3">
                        <div class="card">
                            <div class="card-image">
                                <a href=""><img src="img/gameicon.jpeg"></a>
                            </div>
                        </div>

                    </div>
                </div>
        </div>  
        </div> 
        </div>     
</body>

</html>