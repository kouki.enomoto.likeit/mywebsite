<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>cart</title>
    <!-- home.cssの読み込み -->
    <link href="css/cart.css" rel="stylesheet" type="text/css" />
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <a class="nav-link" href="HomeServlet">ゲームECサイト</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <li class="nav-item">
                    <a class="nav-link" href="UserDetailServlet?id=${userInfo.id}">マイページ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="CartServlet">カート</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="BookmarkServlet">お気に入り</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>

    <div class="container mb-5">
        <div class="col-2 mx-auto mt-5">
            <h3 class="ml-4">カート</h3>
        </div>
        <table>
        <c:forEach var="game" items="${cart}">
            <tr>
                <td><img src="${game.icon}"></td>
                <td>${game.name}</td>
                <td>${game.genreId}</td>
                <td>${game.price}円</td>
                <td><a href="GameDeleteServlet?id=${game.id}" type="button" class="btn btn-primary">削除</a></td>
            </tr>
        </c:forEach>
         
        </table>
        
    </div>
    <div class="row">
        <div class="col s12">
            <div class="col s6 center-align">
                <input class="btn waves-effect waves-light col s6 offset-s3 " type="submit" name="action"
                    value="買い物を続ける">

            </div>
            <div class="col s6 center-align">
            <form action="BuyServlet" method="get">
            
                <input class="btn waves-effect waves-light col s6 offset-s3 " type="submit" 
                   value="購入">
            </form>
            </div>
        </div>
    </div>
    </div>

</body>

</html>