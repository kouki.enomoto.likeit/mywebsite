<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>登録完了</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <a class="nav-link" href="login.html">ゲームECサイト</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">

            </ul>
        </nav>
    </header>


    <div class="container mt-5">
        <div class="col-4 mx-auto">
            <h5>ゲームの登録が完了しました</h5>
        </div>
        <div>
            <div class="col-6 mx-auto row">
                <div class="col-6 center-align">
                    <a href="HomeServlet" class="btn waves-effect waves-light ">TOPページへ戻る</a>
                </div>
                <div class="col-6 center-align">
                    <a href="UserDetailServlet?id=${userInfo.id}" class="btn waves-effect waves-light">マイページへ</a>
                </div>
            </div>
        </div>
        

       </div>

</body></html>
