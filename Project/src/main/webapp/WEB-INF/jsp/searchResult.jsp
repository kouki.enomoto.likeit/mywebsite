<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>検索結果</title>
    <!-- searchResult.cssの読み込み -->
    <link href="css/searchResult.css" rel="stylesheet" type="text/css" />
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <a class="nav-link" href="HomeServlet">ゲームECサイト</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <li class="nav-item">
                    <a class="nav-link" href="UserDetailServlet?id=${userInfo.id}">マイページ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="CartServlet">カート</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="BookmarkServlet">お気に入り</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>

    <div class="container mt-5">
        <!-- 検索結果 -->
        <div class="col-2 mx-auto">
            <h3 class="ml-2">ゲーム検索</h3>
        </div>

        <div class="search col-5 mx-auto row">
        <form action="GameSearchServlet">
            <div class="col-6">
                <input class="search-box" placeholder="キーワード検索" name="search_word">
            </div>
            <div class="col-5">
                <select name="genres" id="genre">
                    <c:forEach var="genreList" items="${genreList}" >
									<option value="${genreList.id}">${genreList.name}</option>
								</c:forEach>
                </select>
                </div>
                <div class="col-1">
                <button type="submit" class="btn btn-primary">検索</button>
            </div>
            </form>
        </div>
        
        <div class="col-2 mx-auto mb-3">
            <h6 class="ml-2">検索結果${gameCount}件</h6>
        </div>

        <div class=" row">

            <table>
              
                <c:forEach var="game" items="${gameList}" varStatus="status">
             
                <tr> 
                    <td><img src="img/${game.icon}"></td>
                    <td><a href="GameDetailServlet?game_id=${game.id}" style="display:block;">${game.name}</a></td>
                    <td>${game.genreId}</td>
                    
                    
                </tr>
                
                </c:forEach>
                
               
            </table>
        </div>







    </div>
    </div>

</body>

</html>