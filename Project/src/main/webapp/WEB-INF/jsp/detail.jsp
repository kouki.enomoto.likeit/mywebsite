<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>detail</title>
    <!-- detail.cssの読み込み -->
    <link href="css/detail.css" rel="stylesheet" type="text/css" />
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <a class="nav-link" href="HomeServlet">ゲームECサイト</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <li class="nav-item">
                    <a class="nav-link" href="UserDetailServlet?id=${userInfo.id}">マイページ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="CartServlet">カート</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="bookmark.html">お気に入り</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="login.html">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>

    <!-- おすすめ商品欄 -->
    <div class="container mt-5">
        <div class="col-3 mx-auto">
            <h3 class="ml-4">${game.name}</h3>
        </div>
        <div>
        <form action="AddGameServlet" method="post">
            <input type="hidden" name="game_id" value="${game.id}">
            <button class="btn btn-primary col-2 cart-button" type="submit">カートに追加</button>
        </form>
        
        </div>
        <div class="row">
            <div class="col-6"><img src="img/${game.icon}"></div>
            <div class="col-6">
                <div class="mt-5">

                </div>
                <div>
                    <h4>${game.price}円</h4>
                </div>
                <div>
                    <a href="myPage.html" class="publisher"></a>
                    <h5 class="product-detail">${game.detail}</h5>
                </div>
                <div class="row"></div>
                <div>
                    <input class="btn btn-primary col-3" value="シューティング">
                
                </div>
            </div>
        </div>
        <div class="row"></div>
        <div class="row">
            <input type="button" class="btn btn-primary col-2 " value="お気に入りに追加">

        </div>
        <div class="row">
       <!--      <p>評価<span class="star5_rating" data-rate="2"></span></p> -->

        </div>

        <div>
            <div class="">
                <h2>開発者からのお知らせ</h2>
                <div class="">
                   ${game.announcement}

                </div>


            </div>
        </div>

        <div class="col s12 m6">
         <!--     <h2>最新のレビュー</h2>
            <div class="card col-6">

                <div class="card-content">
                    <span class="star5_rating" data-rate="4.5"></span>

                    <p class="reviewedname">田中太郎さん</p>
                    <span class="card-title center">面白いゲームでした。</span>
                    <p class="revieweddate">10月1日</p>

                </div>
            </div>
            <div class="card col-6">

                <div class="card-content">
                    <span class="star5_rating" data-rate="4.5"></span>

                    <p class="reviewedname">山田一郎さん</p>
                    <span class="card-title center">難しかったです</span>
                    <p class="revieweddate">9月15日</p>

                </div>
            </div>
        </div>
        <div class="col s12 m6">
            <h2>レビューにご協力ください</h2>

            スコア<span class="star5_rating" data-rate="0"></span>
            <textarea class="review" placeholder="1000文字以内で入力してください"></textarea>
            <input type="button" class="btn btn-primary" value="送信">   -->
        </div>




    </div>

</body>

</html>