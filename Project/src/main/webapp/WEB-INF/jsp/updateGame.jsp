<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>myPage</title>
    <!-- myPage.cssの読み込み -->
    <link href="css/userUpdate.css" rel="stylesheet" type="text/css" />
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <a class="nav-link" href="HomeServlet">ゲームECサイト</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">

                <li class="nav-item">
                    <a class="nav-link" href="UserDetailServlet?id=${userInfo.id}">マイページ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="CartServlet">カート</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="BookmarkServlet">お気に入り</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>
    <div class="container mt-5">
        <!-- ユーザ情報 -->
        <div>
            <div class="col-3 mx-auto">
                <h3>ゲーム情報編集</h3>
            </div>
            <div class="col-8 mx-auto mt-5">
            <form action="GameUpdateServlet" method="post">
            <input type="hidden" name="game_id" value="${game.id}">
                <table>
                    <tbody>
                        <tr>
                            <td width="100" height="50">アイコン</td>
                            <td width="200"><input type="file" name="icon"></td>
                        </tr>
                        <tr>
                            <td width="100" height="50">タイトル</td>
                            <td width="200"><input type="text" name="game_name" value="${game.name}"></td>
                        </tr>
                        <tr>
                            <td height="50">価格</td>
                            <td><input type="text" name="price" value="${game.price}">円</td>
                        </tr>
                        <tr>
                            <td class="center" height="50">詳細</td>
                            <td><textarea name="detail" class="">${game.detail}</textarea></td>
                        </tr>
                        <tr>
                            <td class="center" height="50">お知らせ</td>
                            <td><textarea name="announcement" class="">${game.announcement}</textarea></td>
                        </tr>
                        <tr>
                        <td class="center">ジャンル
                        </td>
                        <td class="center">
                            <select name="genre_id" id="genre">
                                <c:forEach var="genreList" items="${genreList}" >
									<option value="${genreList.id}">${genreList.name}</option>
								</c:forEach>
                            </select>
                        </td>
                        </tr>


                    </tbody>
                </table>
                
                <div class="col-3 mx-auto"><button type="submit" class="btn btn-primary ml-5" >更新</button></div>
				</form>>
            </div>
        </div>

        <!-- 購入履歴 -->

    </div>

</body>

</html>