<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>myPage</title>
    <!-- myPage.cssの読み込み -->
    <link href="css/myPage.css" rel="stylesheet" type="text/css" />
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
    <link href="css/table.css" rel="stylesheet" type="text/css">
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js" type="text/javascript"></script>
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <a class="nav-link" href="home.html">ゲームECサイト</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">

                <li class="nav-item">
                    <a class="nav-link" href="UserDetailServlet?id=${userInfo.id}">マイページ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="CartServlet">カート</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="bookmark.html">お気に入り</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="login.html">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>
    <div class="container mt-5">
        <!-- ユーザ情報 -->
        <div>
            <div class="col-12 mx-auto row">



                <div class="col-4 center-align">
                    <p style="font-size:28px;">アクションゲーム</p>
                </div>
                <div class="col-6 center-align">
                    <table>
                        <tr>
                            <th>価格</th>
                            <th>販売数</th>
                            <th>売上</th>
                        </tr>
                        <tr>
                            <th>1,000円</th>
                            <th>1,000</th>
                            <th>1,000,000円</th>
                        </tr>
                    </table>
                </div>
                <div class="col-2 center-align">
                    <input type="button" class="btn btn-primary" value="編集">
                    <input type="button" class="btn btn-primary" value="削除">
                </div>

            </div>
        </div>
        <div class="col mx-auto">
            <h3> 総合評価</h3>
             <h2><span class="star5_rating" data-rate="4.5"></span></h2>
 
 
         </div>

        <div class="col mx-auto">
            <h2>購入者の属性</h2>
            <div class="col-6 mx-auto">
                <canvas id="graph-area" height="450" width="600">
                    <script type="text/javascript">


                        var barChartData = {
                            labels: ["10代", "20代", "30代", "40代", "50代以上"],
                            datasets: [
                                {
                                    fillColor: "rgba(240,128,128,0.6)",    // 塗りつぶし色
                                    strokeColor: "rgba(240,128,128,0.9)",  // 枠線の色
                                    highlightFill: "rgba(255,64,64,0.75)",  // マウスが載った際の塗りつぶし色
                                    highlightStroke: "rgba(255,64,64,1)",   // マウスが載った際の枠線の色
                                    data: [150, 80, 40, 45, 60, 70, 80]     // 各棒の値(カンマ区切りで指定)
                                },
                                {
                                    fillColor: "rgba(151,187,205,0.6)",
                                    strokeColor: "rgba(151,187,205,0.9)",
                                    highlightFill: "rgba(64,96,255,0.75)",
                                    highlightStroke: "rgba(64,96,255,1)",
                                    data: [50, 120, 70, 60, 55, 40, 25]
                                }
                            ]

                        }

                        // ▼上記のグラフを描画するための記述
                        window.onload = function () {
                            var ctx = document.getElementById("graph-area").getContext("2d");
                            window.myBar = new Chart(ctx).Bar(barChartData);
                        }

                    </script>
                </canvas>

            </div>

        </div>


       


    </div>

</body>

</html>