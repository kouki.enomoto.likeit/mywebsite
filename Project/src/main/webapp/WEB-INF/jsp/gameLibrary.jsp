<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>myPage</title>
    <!-- searchResult.cssの読み込み -->
    <link href="css/searchResult.css" rel="stylesheet" type="text/css" />
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <a class="nav-link" href="home.html">ゲームECサイト</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <li class="nav-item">
                    <a class="nav-link" href="UserDetailServlet?id=${userInfo.id}">マイページ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="CartServlet">カート</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="bookmark.html">お気に入り</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="login.html">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>

    <div class="container mt-5">
        <!-- 検索結果 -->
        <div class="col-4 mx-auto">
            <h3 class="ml-2">所持ゲーム一覧</h3>
        </div>
        <div class="col-2 mx-auto mb-3">
            <h6 class="ml-2">所持数:3</h6>
        </div>
        <div class=" row">

            <table>
                <tr>
                    <th>アイコン</th>
                    <th>ゲーム名</th>
                    <th>ジャンル</th>
                    <th>評価</th>
                    <th>価格</th>
                </tr>
                <tr>
                    <td><img src="img/game1.jpg"></td>
                    <td>面白いゲーム1</td>
                    <td>アクション</td>
                    <td>3.6</td>
                    <td>2000円</td>
                </tr>
                <tr>
                    <td><img src="img/game2.jpg"></td>
                    <td>面白いゲーム2</td>
                    <td>アドベンチャー</td>
                    <td>4.2</td>
                    <td>6000円</td>
                </tr>
                <tr>
                    <td><img src="img/game3.jpg"></td>
                    <td>面白いゲーム3</td>
                    <td>ホラー</td>
                    <td>4.8</td>
                    <td>1000円</td>
                </tr>
            </table>
          
          
        </div>
    </div>

</body>

</html>