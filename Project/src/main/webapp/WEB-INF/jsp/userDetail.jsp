<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>マイページ</title>
    <!-- myPage.cssの読み込み -->
    <link href="css/myPage.css" rel="stylesheet" type="text/css" />
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <a class="nav-link" href="HomeServlet">ゲームECサイト</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">

                <li class="nav-item">
                    <a class="nav-link" href="UserDetailServlet?id=${userInfo.id}">マイページ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="CartServlet">カート</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="BookmarkServlet">お気に入り</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>
    <div class="container mt-5">
        <!-- ユーザ情報 -->
        <div>
            <div class="col-12 mx-auto row">
                <div class="col-4 center-align">
                    <img class="icon" src="img/${userDetail.icon}">
                </div>
                <div class="col-6 center-align">
                    <p style="font-size:28px;">${userDetail.name}</p>
                </div>
                <div class="col-2 center-align">
                    <a class="btn btn-primary" href="UserUpdateServlet?id=${userInfo.id}">ユーザー情報編集</a>
                </div>
            </div>
        </div>
        
 <!--自己紹介文 -->
            <div class="col-6 mx-auto userInfo">
                ${userDetail.profile}
               

            </div>
        </div>

     <!--ゲームライブラリ-->  
        <div class="buy col-8 mx-auto mt-5">
            <table>
                <thead>
                    <tr>
                        <th style="width: 10%"></th>
                        <th class="center">ゲームライブラリ</th>
                        
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <td class="center"><a href="buyDetail.html" class="btn btn-primary">プレイ</a></td>
                        <td class="center">アクションゲーム2</td>
                        <td class="center"><a href="buyDetail.html" class="btn btn-primary">詳細</a></td>
                       
                        
                       
                           
                    </tr>
                    <tr>
                        <td class="center"><a href="buyDetail.html" class="btn btn-primary">プレイ</a></td>
                        <td class="center">アクションゲーム2</td>
                        <td class="center"><a href="buyDetail.html" class="btn btn-primary">詳細</a></td>
                       
                        
                       
                           
                    </tr>
                    <tr>
                        <td class="right"><a href="gamelibrary.html" class="btn waves-effect waves-light ">さらに見る</a></td>
                    </tr>

                </tbody>
            </table>
        </div>
        <div class="buy col-8 mx-auto mt-5">
            <table>
                <thead>
                    <tr>
                        <th style="width: 10%"></th>
                        <th class="center">あなたが販売中のゲーム</th>
                        
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <td class="center"><a href="buyDetail.html" class="btn btn-primary">プレイ</a></td>
                        <td class="center">アクションゲーム2</td>
                        <td class="center"><a href="buyDetail.html" class="btn btn-primary">詳細</a></td>
                       
                        
                       
                           
                    </tr>
                    <tr>
                        <td class="center"><a href="buyDetail.html" class="btn btn-primary">プレイ</a></td>
                        <td class="center">アクションゲーム2</td>
                        <td class="center"><a href="buyDetail.html" class="btn btn-primary">詳細</a></td>
                       
                        
                       
                           
                    </tr>
                    <tr>
                        <td class="right"><a href="gamelibrary.html" class="btn waves-effect waves-light ">さらに見る</a></td>
                    </tr>

                </tbody>
            </table>
        </div>
        <div class="col-12 mx-auto row">
            <div class="col-6 center-align">
            <button class="btn btn-primary" onclick="location.href='RegisterGameServlet'">ゲームを登録する</button></div>
            <div class="col-4 center-align">
                ウォレット:10000円</div>
            <div class="col-2 center-align">

<input type="button" class="btn btn-primary" value="収益受け取り">
           </div></div>
    

</body>

</html>