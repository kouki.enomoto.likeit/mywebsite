<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>新規登録</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <a class="nav-link" href="HomeServlet">ゲームECサイト</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">

            </ul>
        </nav>
    </header>




    <div class="container mt-5">
        <div class="col-2 mx-auto">
            <h3>新規登録</h3>
        </div>
        <!--   登録フォーム   -->
        <form action="UserAddServlet" method="post">
        <div class="col-8 mx-auto ">
            <table>
                <tbody>
                    <tr height="80">
                        <td class="center" width="200">名前</td>
                        <td class="center"><input type="text" name="user-name" id="inputName"></td>
                    </tr>
                    <tr height="80">
                        <td class="center">ログインID</td>
                        <td class="center"><input type="text" name="user-loginid" id="imputLogignId"></td>
                    </tr>
                    <tr height="80">
                        <td class="center">パスワード</td>
                        <td class="center"><input type="password" name="password" id="inputPassword"></td>
                    </tr>
                    <tr height="80">
                        <td class="center">パスワード(確認用)</td>
                        <td class="center"><input type="password" name="password-confirm"></td>
                    </tr>
                    <tr height="80">
                        <td class="center">性別</td>
                        <td class="center">
                            <input type="radio" name="is-man" value="0">　男
                            　　　
                            <input type="radio" name="is-man" value="1">　女
                        </td>


                    <tr height="60">
                        <td class="center">生年月日</td>
                        <td class="center"><input type="date" name="birth-date"></td>
                    </tr>
                    <tr height="80">

                        <td class="center">ジャンル</td>
                        <td class="center">
                            <select name="genre_id">
                                
								<c:forEach var="genreList" items="${genreList}" >
									<option value="${genreList.id}">${genreList.name}</option>
								</c:forEach>
                            </select>
                        </td>

                       

                    </tr>
                </tbody>
            </table>
            <div class="col-3 mx-auto mt-3">
                <button type="submit" class="btn btn-primary" >登録</button>
            </div>
            </div>
            </form>
        </div>
    

</body>

</html>