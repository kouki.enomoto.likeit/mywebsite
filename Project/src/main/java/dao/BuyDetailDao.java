package dao;

import java.sql.Connection;
import java.sql.Date;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import dao.DBManager;
import model.BuyInfo;
import model.BuyDetail;

public class BuyDetailDao {

  public static void insertBuyInfo(BuyInfo buyInfo) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();
      st = con.prepareStatement(
          "INSERT INTO buy_info(user_id, buy_method, total_price, buy_date) VALUES(?,?,?,?)");
      st.setInt(1, buyInfo.getUserId());
      st.setInt(2, buyInfo.getBuyMethod());
      st.setInt(3, buyInfo.getTotalPrice());
      Date buyDate = (Date) buyInfo.getBuyDate();
      st.setDate(4, buyDate);
      st.executeUpdate();


    } catch (SQLException e) {

      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }

  }

  public static void insertBuydetail(BuyDetail buyDetail) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();
      st = con.prepareStatement("INSERT INTO buy_detail(buy_info_id,game_id) VALUES(?,?)");
      st.setInt(1, buyDetail.getBuyInfo());
      st.setInt(2, buyDetail.getGameId());
      st.executeUpdate();


    } catch (SQLException e) {

      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }
}
