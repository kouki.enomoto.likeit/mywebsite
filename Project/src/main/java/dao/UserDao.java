package dao;

import model.User;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class UserDao {


  public User findByLoginInfo(String loginId, String password) {

    Connection conn = null;
    try {
      conn = DBManager.getConnection();
      
      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";
      
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();
      
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String _loginId = rs.getString("login_id");
      String _password = rs.getString("password");
      String name = rs.getString("user_name");
      boolean isMan = rs.getBoolean("isMan");
      Date birthDate = rs.getDate("birth_date");
      String profile = rs.getString("profile");
      String icon = rs.getString("icon");
      int genreId = rs.getInt("genre_id");
      return new User(id, _loginId, _password, name, isMan, birthDate, profile, icon, genreId);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }

      }
    }
  }

  public void addUser(String loginId, String password, String name, String isMan, String birthDate,
      String genre) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();
      String sql =
          "INSERT INTO user(login_id, password, user_name, isMan, birth_date, genre_id)VALUES(?,?,?,?,?,?)";
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      pStmt.setString(3, name);
      pStmt.setString(4, isMan);
      pStmt.setString(5, birthDate);
      pStmt.setString(6, genre);
      int rs = pStmt.executeUpdate();




    } catch (SQLException e) {
      e.printStackTrace();
    } finally {

      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }


  }

  public void updateUser(int id, String loginId, String password, String name,
      String profile, int genreId) {

    Connection conn = null;
    try {
      conn = DBManager.getConnection();
      String sql =
          "UPDATE user SET user_name =?, password =?, login_id =?, profile =?, genre_id =? WHERE id =?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, name);
      pStmt.setString(2, password);
      pStmt.setString(3, loginId);
      pStmt.setString(4, profile);
      pStmt.setInt(5, genreId);
      pStmt.setInt(6, id);
      int result = pStmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();
    } finally {

      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    
  }

  public User findById(int userId) {
    Connection conn = null;
    try {

      conn = DBManager.getConnection();
      String sql = "SELECT * FROM user WHERE id =?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, userId);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }
      int id = rs.getInt("id");
      String _loginId = rs.getString("login_id");
      String _password = rs.getString("password");
      String name = rs.getString("user_name");
      boolean Isman = rs.getBoolean("isMan");
      Date birthDate = rs.getDate("birth_date");
      String profile = rs.getString("profile");
      String icon = rs.getString("icon");
      int genreId = rs.getInt("genre_id");
      return new User(id, _loginId, _password, name, Isman, birthDate, profile, icon, genreId);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {

      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }

  }

}
