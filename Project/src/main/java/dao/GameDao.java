package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import dao.DBManager;
import model.Game;
import model.Genre;
import model.User;


public class GameDao {

  public static Game getGameByGameId(int gameId) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("SELECT * FROM game WHERE id = ?");
      st.setInt(1, gameId);
      ResultSet rs = st.executeQuery();

      Game game = new Game();
      if (rs.next()) {
        game.setId(rs.getInt("id"));
        game.setUserId(rs.getInt("user_id"));
        game.setName(rs.getString("name"));
        game.setIcon(rs.getString("icon"));
        game.setPrice(rs.getInt("price"));
        game.setDetail(rs.getString("detail"));
        game.setAnnouncement(rs.getString("announcement"));
        game.setGenreId(rs.getInt("genre_id"));

      }
      return game;

    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }

  public void addGame(String userId, String name, String icon, String price, String detail,
      String genreId) {

    Connection conn = null;
    try {
      conn = DBManager.getConnection();

      String sql =
          "INSERT INTO game(user_id, name, icon, price, detail, genre_id)VALUES(?,?,?,?,?,?)";
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, userId);
      pStmt.setString(2, name);
      pStmt.setString(3, icon);
      pStmt.setString(4, price);
      pStmt.setString(5, detail);
      pStmt.setString(6, genreId);
      int rs = pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }

      }
    }

  }

  public static Game getGameByUserId(int userId) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("SELECT * FROM game WHERE user_id = ?");
      st.setInt(1, userId);
      ResultSet rs = st.executeQuery();

      Game game = new Game();
      if (rs.next()) {
        game.setId(rs.getInt("id"));
        game.setUserId(rs.getInt("user_id"));
        game.setName(rs.getString("name"));
        game.setIcon(rs.getString("icon"));
        game.setPrice(rs.getInt("price"));
        game.setDetail(rs.getString("detail"));
        game.setAnnouncement(rs.getString("announcement"));
        game.setGenreId(rs.getInt("genre_id"));

      }
      return game;

    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }

  public static ArrayList<Game> getGameByGenreId(int genreId) throws SQLException{
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("SELECT * FROM game WHERE genre_id = ?");
      st.setInt(1, genreId);
      ResultSet rs = st.executeQuery();
      
      ArrayList<Game> gameList = new ArrayList<Game>();
      
      while(rs.next()) {
      Game game = new Game();
      if (rs.next()) {
        game.setId(rs.getInt("id"));
        game.setUserId(rs.getInt("user_id"));
        game.setName(rs.getString("name"));
        game.setIcon(rs.getString("icon"));
        game.setPrice(rs.getInt("price"));
        game.setDetail(rs.getString("detail"));
        game.setAnnouncement(rs.getString("announcement"));
        game.setGenreId(rs.getInt("genre_id"));
        gameList.add(game);
      }
      }
      return gameList;

    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
  }
}

public static ArrayList<Game> getRandGame(int limit) throws SQLException {

  Connection con = null;
  PreparedStatement st = null;
  try {
    con = DBManager.getConnection();

    st = con.prepareStatement("SELECT * FROM game ORDER BY RAND() LIMIT ? ");
    st.setInt(1, limit);

    ResultSet rs = st.executeQuery();

    ArrayList<Game> gameList = new ArrayList<Game>();

    while (rs.next()) {
      Game game = new Game();
      game.setId(rs.getInt("id"));
      game.setUserId(rs.getInt("user_id"));
      game.setName(rs.getString("name"));
      game.setIcon(rs.getString("icon"));
      game.setPrice(rs.getInt("price"));
      game.setGenreId(rs.getInt("genre_id"));
      gameList.add(game);

    }

    return gameList;
  } catch (SQLException e) {
    System.out.println(e.getMessage());
    throw new SQLException(e);
  } finally {
    if (con != null) {
      con.close();
    }
  }
}

public void updateGame(int id, String name, String price, String detail, String announcement,
    String icon, int genreId) {
  Connection conn = null;
  try {
    conn = DBManager.getConnection();
    String sql =
        "UPDATE game SET name =?, price =?, detail =?, announcement =?, icon =?, genre_id =? WHERE id =?";
    PreparedStatement pStmt = conn.prepareStatement(sql);
    pStmt.setString(1, name);
    pStmt.setString(2, price);
    pStmt.setString(3, detail);
    pStmt.setString(4, announcement);
    pStmt.setString(5, icon);
    pStmt.setInt(6, genreId);
    pStmt.setInt(7, id);
    int result = pStmt.executeUpdate();


  } catch (SQLException e) {
    e.printStackTrace();
  } finally {

    if (conn != null) {
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }

}

public static ArrayList<Game> getGamesByGameName(String searchWord, int pageNum, int pageMaxItemCount) throws SQLException{
  Connection conn = null;
  PreparedStatement st = null;
  try {
    int startGameNum = (pageNum - 1) * pageMaxItemCount;
    conn = DBManager.getConnection();
    if(searchWord.length() ==0) {
    

    st = conn.prepareStatement("SELECT * FROM game ORDER BY id ASC LIMIT ?,? ");
    st.setInt(1,startGameNum);
    st.setInt(2, pageMaxItemCount);
    }else {
      st = conn.prepareStatement("SELECT * FROM game WHERE name LIKE ? ORDER BY id ASC LIMIT ?,? ");
      st.setString(1,"%"+ searchWord +"%");
      st.setInt(2, startGameNum);
      st.setInt(3, pageMaxItemCount);
    }
    ResultSet rs = st.executeQuery();
    ArrayList<Game> gameList = new ArrayList<Game>();
    while (rs.next()) {
      Game game = new Game();
      game.setId(rs.getInt("id"));
      game.setName(rs.getString("name"));
      game.setUserId(rs.getInt("user_id"));
      game.setIcon(rs.getString("icon"));
      game.setPrice(rs.getInt("price"));
      game.setDetail(rs.getString("detail"));
      game.setAnnouncement(rs.getString("announcement"));
      game.setGenreId(rs.getInt("genre_id"));
      gameList.add(game);
    }
    return gameList;
    
  }catch(SQLException e){
    throw new SQLException(e);
  } finally {
    if (conn != null) {
      conn.close();
  }
}
}

public static double getGameCount(String searchWord) throws SQLException {
  Connection conn = null;
  PreparedStatement st = null;
  try {
    conn = DBManager.getConnection();
    st = conn.prepareStatement("select count(*) as cnt from game where name like ?");
    st.setString(1, "%" + searchWord + "%");
    ResultSet rs = st.executeQuery();
    double count = 0.0;
    while (rs.next()) {
      count = Double.parseDouble(rs.getString("cnt"));
    }
    return count;
  } catch (Exception e) {
    System.out.println(e.getMessage());
    throw new SQLException(e);
  } finally {
    if (conn != null) {
      conn.close();
    }
  }
}

}
