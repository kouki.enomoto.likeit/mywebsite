package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import dao.DBManager;
import model.BuyMethod;
import model.Game;


public class BuyMethodDao {

  public static ArrayList<BuyMethod> getAllBuyMethod() throws SQLException {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();
      PreparedStatement pStmt = conn.prepareStatement("SELECT * FROM buy_method");
      ResultSet rs = pStmt.executeQuery();

      ArrayList<BuyMethod> buyMethodList = new ArrayList<BuyMethod>();
      while (rs.next()) {
        BuyMethod bm = new BuyMethod();
        bm.setId(rs.getInt("id"));
        bm.setName(rs.getString("name"));
        buyMethodList.add(bm);
      }
      return buyMethodList;

    } catch (SQLException e) {

      throw new SQLException(e);
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
  }

  public static BuyMethod getBuyMethodByID(int buyMethodId) throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("SELECT * FROM buy_method WHERE id = ?");
      st.setInt(1, buyMethodId);

      ResultSet rs = st.executeQuery();

      BuyMethod buyMethod = new BuyMethod();
      while (rs.next()) {
        buyMethod.setId(rs.getInt("id"));
        buyMethod.setName(rs.getString("name"));
      }

      

      return buyMethod;
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }

  public static int getTotalGamePrice(ArrayList<Game> gameList) {
    int total = 0;
    for (Game game : gameList) {
      total += game.getPrice();
    }
    return total;
  }

  public static Object cutSessionAttribute(HttpSession session, String str) {
    Object test = session.getAttribute(str);
    session.removeAttribute(str);
    return test;
  }

}
