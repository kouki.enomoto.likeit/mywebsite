package dao;

import model.Genre;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import dao.DBManager;


public class GenreDao {
  public static ArrayList<Genre> getAllGenres() throws SQLException {
    Connection con = null;
    PreparedStatement st = null;
    try {
      con = DBManager.getConnection();

      st = con.prepareStatement("SELECT * FROM genre");

      ResultSet rs = st.executeQuery();

      ArrayList<Genre> genreList = new ArrayList<Genre>();
      while (rs.next()) {
        Genre genre = new Genre();
        genre.setId(rs.getInt("id"));
        genre.setName(rs.getString("name"));
        genreList.add(genre);
      }

      return genreList;
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }

}
