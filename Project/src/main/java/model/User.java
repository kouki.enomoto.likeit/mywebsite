package model;

import java.io.Serializable;
import java.util.Date;


public class User implements Serializable {
  private int id;
  private String loginId;
  private String password;
  private String name;
  private boolean isMan;
  private Date birthDate;
  private String profile;
  private String icon;
  private int genreId;

  public User(int id, String loginId, String password, String name, boolean isMan, Date birthDate,
      String profile, String icon, int genreId) {
    this.id = id;
    this.loginId = loginId;
    this.password = password;
    this.name = name;
    this.isMan = isMan;
    this.birthDate = birthDate;
    this.profile = profile;
    this.icon = icon;
    this.genreId = genreId;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getLoginId() {
    return loginId;
  }

  public void setLoginId(String loginId) {
    this.loginId = loginId;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean isMan() {
    return isMan;
  }

  public void setMan(boolean isMan) {
    this.isMan = isMan;
  }

  public Date getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(Date birthDate) {
    this.birthDate = birthDate;
  }

  public String getProfile() {
    return profile;
  }

  public void setProfile(String profile) {
    this.profile = profile;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  public int getGenreId() {
    return genreId;
  }

  public void setGenreId(int genreId) {
    this.genreId = genreId;
  }

}
