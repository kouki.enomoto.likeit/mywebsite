package model;

import java.io.Serializable;
import java.util.Date;


public class BuyInfo implements Serializable {

  private int id;
  private int userId;
  private int buyMethod;
  private int totalPrice;
  private Date buyDate;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public int getBuyMethod() {
    return buyMethod;
  }

  public void setBuyMethod(int buyMethod) {
    this.buyMethod = buyMethod;
  }

  public int getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(int totalPrice) {
    this.totalPrice = totalPrice;
  }

  public Date getBuyDate() {
    return buyDate;
  }

  public void setBuyDate(Date buyDate) {
    this.buyDate = buyDate;
  }



}
