package model;

import java.io.Serializable;

public class BuyDetail implements Serializable {

  private int id;
  private int buyInfo;
  private int gameId;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getBuyInfo() {
    return buyInfo;
  }

  public void setBuyInfo(int buyInfo) {
    this.buyInfo = buyInfo;
  }

  public int getGameId() {
    return gameId;
  }

  public void setGameId(int gameId) {
    this.gameId = gameId;
  }


}
