package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.GenreDao;
import dao.GameDao;
import model.Genre;
import model.User;
import model.Game;
import javax.servlet.http.Part;

/**
 * Servlet implementation class GameUpdateServlet
 */
@WebServlet("/GameUpdateServlet")
public class GameUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GameUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      HttpSession session = request.getSession();
      User userInfo = (User) session.getAttribute("userInfo");
      if (userInfo == null) {
        response.sendRedirect("HomeServlet");
        return;
      }

      try {
        ArrayList<Genre> genreList = GenreDao.getAllGenres();
        request.setAttribute("genreList", genreList);

      } catch (SQLException e) {
        e.printStackTrace();

      }
      String id = request.getParameter("id");
      int gameId = Integer.valueOf(id);
      GameDao gameDao = new GameDao();

      try {
        Game game = gameDao.getGameByGameId(gameId);
        request.setAttribute("game", game);

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/gameUpdate.jsp");
        dispatcher.forward(request, response);

      } catch (SQLException e) {
        // TODO 自動生成された catch ブロック
        e.printStackTrace();
      }
      



	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      String icon = request.getParameter("icon");

      String id = request.getParameter("game_id");
      int gameId = Integer.valueOf(id);
      String name = request.getParameter("game_name");
      String price = request.getParameter("price");
      String detail = request.getParameter("detail");
      String announcement = request.getParameter("announcement");
      String genre = request.getParameter("genre_id");
      int genreId = Integer.valueOf(genre);


      GameDao gameDao = new GameDao();
      gameDao.updateGame(gameId, name, price, detail, announcement, icon, genreId);
      response.sendRedirect("HomeServlet");

	}

}
