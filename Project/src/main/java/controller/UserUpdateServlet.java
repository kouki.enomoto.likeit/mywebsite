package controller;

import java.io.IOException;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Genre;
import model.User;
import dao.GenreDao;
import dao.UserDao;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.Part;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")

public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      HttpSession session = request.getSession();
      User userInfo = (User) session.getAttribute("userInfo");
      if (userInfo == null) {
        response.sendRedirect("HomeServlet");
        return;
      }

      try {
        ArrayList<Genre> genreList = GenreDao.getAllGenres();
        request.setAttribute("genreList", genreList);


      } catch (SQLException e) {
        // TODO 自動生成された catch ブロック
        e.printStackTrace();

      }

      String id = request.getParameter("id");
      int userId = Integer.valueOf(id);
      UserDao userDao = new UserDao();
      User user = userDao.findById(userId);
      request.setAttribute("userUpdate", user);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
      dispatcher.forward(request, response);
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");

      // part = request.getPart("icon");
      String id = request.getParameter("user-id");
      int userId = Integer.valueOf(id);
      String inputLoginId = request.getParameter("login-id");
      String password = request.getParameter("password");
      String name = request.getParameter("name");
      String profile = request.getParameter("profile");
      // String icon = Paths.get(part.getSubmittedFileName()).getFileName().toString();
      String confirmPassword = request.getParameter("confirm-password");
      String genreId = request.getParameter("genre_id");
      int genreid = Integer.valueOf(genreId);


      UserDao userDao = new UserDao();
      userDao.updateUser(userId, inputLoginId, password, name, profile, genreid);

      response.sendRedirect("HomeServlet");



	}

}
