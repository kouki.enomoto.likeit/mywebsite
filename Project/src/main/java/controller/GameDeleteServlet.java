package controller;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Game;

/**
 * Servlet implementation class GameDeleteServlet
 */
@WebServlet("/GameDeleteServlet")
public class GameDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GameDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      HttpSession session = request.getSession();
		
      try {
        ArrayList<Game> cart = (ArrayList<Game>) session.getAttribute("cart");

        String id = request.getParameter("id");

        for (Game inCartGame : cart) {
          if (inCartGame.getId() == Integer.parseInt(id)) {
            cart.remove(inCartGame);
            break;
          }
        }
        request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp").forward(request, response);
      } catch (Exception e) {
        e.printStackTrace();

      }

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      // TODO Auto-generated method stub
      doGet(request, response);
    }

}
