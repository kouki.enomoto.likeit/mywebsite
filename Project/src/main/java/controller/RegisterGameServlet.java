package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.GameDao;
import dao.GenreDao;
import model.Genre;

/**
 * Servlet implementation class ResisterGameServlet
 */
@WebServlet("/RegisterGameServlet")
public class RegisterGameServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterGameServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
        session.getAttribute("userInfo");

        try {
          ArrayList<Genre> genreList = GenreDao.getAllGenres();
          request.setAttribute("genreList", genreList);


        } catch (SQLException e) {
          // TODO 自動生成された catch ブロック
          e.printStackTrace();

        }

        RequestDispatcher dispatcher =
            request.getRequestDispatcher("/WEB-INF/jsp/registerGame.jsp");
        dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");
      String userId = request.getParameter("user-id");
      String name = request.getParameter("game-name");
      String icon = request.getParameter("icon");
      String price = request.getParameter("price");
      String detail = request.getParameter("detail");
      String genreId = request.getParameter("genre-id");
      GameDao gameDao = new GameDao();

      // 例外処理後述

      gameDao.addGame(userId, name, icon, price, detail, genreId);

      response.sendRedirect("HomeServlet");
	}

}
