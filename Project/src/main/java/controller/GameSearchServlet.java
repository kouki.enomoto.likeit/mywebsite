package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Game;
import dao.GameDao;


/**
 * Servlet implementation class GameSerchServlet
 */
@WebServlet("/GameSearchServlet")
public class GameSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    final static int PAGE_MAX_GAME_COUNT = 10;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GameSearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      HttpSession session = request.getSession();
      try {
        String searchWord = request.getParameter("search_word");
        int pageNum = 1;
        session.setAttribute("searchWord", searchWord);
        ArrayList<Game> searchResultGameList =
            GameDao.getGamesByGameName(searchWord, pageNum, PAGE_MAX_GAME_COUNT);

        double gameCount = GameDao.getGameCount(searchWord);
        int pageMax = (int) Math.ceil(gameCount / PAGE_MAX_GAME_COUNT);
        request.setAttribute("gameCount", (int) gameCount);
        request.setAttribute("pageMax", pageMax);
        request.setAttribute("pageNum", pageNum);
        request.setAttribute("gameList", searchResultGameList);
        RequestDispatcher dispatcher =
            request.getRequestDispatcher("/WEB-INF/jsp/searchResult.jsp");
        dispatcher.forward(request, response);

      } catch (Exception e) {
        e.printStackTrace();
      }

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
