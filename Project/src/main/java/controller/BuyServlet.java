package controller;

import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.BuyMethodDao;
import dao.BuyDetailDao;
import java.util.ArrayList;
import model.Game;
import model.BuyMethod;
import model.BuyInfo;



/**
 * Servlet implementation class BuyServlet
 */
@WebServlet("/BuyServlet")
public class BuyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		try {

		  ArrayList<Game> cart = (ArrayList<Game>)session.getAttribute("cart");
		  
          if (session.getAttribute("userInfo") == null) {
            response.sendRedirect("LoginServlet");
          } else if (cart.size() == 0) {
            request.getRequestDispatcher("/WEB-INF/jsp/home.jsp").forward(request, response);
          } else {
            ArrayList<BuyMethod> buyMethod = BuyMethodDao.getAllBuyMethod();
            request.setAttribute("buyMethod", buyMethod);

            request.getRequestDispatcher("/WEB-INF/jsp/buyConfirm.jsp").forward(request, response);
            
          }

		  
		}catch (Exception e) {
          e.printStackTrace();
		}
      }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
        try {
          int inputBuyMethodId = Integer.parseInt(request.getParameter("buy_method"));
          BuyMethod buyMethod = BuyMethodDao.getBuyMethodByID(inputBuyMethodId);
          String buyMethodName = buyMethod.getName();

          ArrayList<Game> cart = (ArrayList<Game>) session.getAttribute("cart");
          int totalPrice = BuyMethodDao.getTotalGamePrice(cart);
          BuyInfo buyInfo = new BuyInfo();
          buyInfo.setUserId((int) session.getAttribute("userId"));
          buyInfo.setTotalPrice(totalPrice);
          buyInfo.setBuyMethod(buyMethod.getId());
          Date buyDate = new Date();
          buyInfo.setBuyDate(buyDate);
          BuyDetailDao.insertBuyInfo(buyInfo);
          request.setAttribute("buyInfo", buyInfo);


          request.getRequestDispatcher("/WEB-INF/jsp/buyResult.jsp").forward(request, response);

    } catch (Exception e) {
      e.printStackTrace();
    }
	}

}
