package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.GameDao;
import model.Game;
import java.util.ArrayList;

/**
 * Servlet implementation class AddGameServlet
 */
@WebServlet("/AddGameServlet")
public class AddGameServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddGameServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	HttpSession session = request.getSession();
	
	try {
      int id = Integer.parseInt(request.getParameter("game_id"));
	  Game game = GameDao.getGameByGameId(id);
	  request.setAttribute("game", game);
	  
      ArrayList<Game> cart = (ArrayList<Game>) session.getAttribute("cart");
	  
	  if(cart == null) {
        cart = new ArrayList<Game>();

      }
	    cart.add(game);

        session.setAttribute("cart", cart);
        request.setAttribute("cartActionMessage", "商品追加しました");
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
        dispatcher.forward(request, response);


	}catch (Exception e) {
      e.printStackTrace();
    }
  }
}
