package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.GenreDao;
import dao.UserDao;
import model.User;
import model.Genre;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public UserAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

      try {
        ArrayList<Genre> genreList = GenreDao.getAllGenres();
        request.setAttribute("genreList", genreList);


      } catch (SQLException e) {
        // TODO 自動生成された catch ブロック
        e.printStackTrace();

      }
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
        String inputLoginId = request.getParameter("user-loginid");
        String password = request.getParameter("password");
        String name = request.getParameter("user-name");
        String confirmPassword = request.getParameter("password-confirm");
        String isMan = request.getParameter("is-man");
        String birthDate = request.getParameter("birth-date");
        String genre = request.getParameter("genre_id");

        UserDao userDao = new UserDao();
        /*
         * 例外処理 後で見る List<User> userList = userDao.findAll(); boolean sameLoginId = false; for (User
         * user : userList) { if (inputLoginId.equals(user.getLoginId())) { sameLoginId = true; } }
         * 
         * 
         * 
         * if (inputLoginId.equals("") || password.equals("") || name.equals("") ||
         * birthDate.equals("") || confirmPassword.equals("") || !(password.equals(confirmPassword))
         * || sameLoginId) { request.setAttribute("errMsg", "入力された内容は正しくありません");
         * request.setAttribute("inputLoginId", inputLoginId); request.setAttribute("name", name);
         * request.setAttribute("birthDate", birthDate); RequestDispatcher dispatcher =
         * request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp"); dispatcher.forward(request,
         * response); return; }
         */


        userDao.addUser(inputLoginId, password, name, isMan, birthDate, genre);
        response.sendRedirect("LoginServlet");

}

  }
