CREATE DATABASE mywebsite default character set utf8;
USE mywebsite;

CREATE TABLE user(
id int PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT,
login_id VARCHAR(191) UNIQUE NOT NULL,
password VARCHAR(256) NOT NULL,
user_name VARCHAR(256) NOT NULL,
isMan BOOLEAN NOT NULL default 0,
birth_date DATE NOT NULL,
profile VARCHAR(200),
icon VARCHAR(256),
genre_id int NOT NULL,
FOREIGN KEY(genre_id) REFERENCES genre(id)
);

CREATE TABLE game(
id int PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT,
user_id int NOT NULL,
name VARCHAR(255) NOT NULL,
icon VARCHAR(255),
price int NOT NULL,
detail VARCHAR(1000),
announcement VARCHAR(1000),
genre_id int NOT NULL,
FOREIGN KEY(user_id) REFERENCES user(id),
FOREIGN KEY(genre_id) REFERENCES genre(id)
);

CREATE TABLE buy_method(
id int PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT,
name VARCHAR(255)
);

CREATE TABLE buy_info(
id int PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT,
user_id int NOT NULL,
buy_method int NOT NULL,
total_price int NOT NULL,
buy_date DATE NOT NULL,
FOREIGN KEY(user_id) REFERENCES user(id),
FOREIGN KEY(buy_method) REFERENCES buy_method(id)
);

CREATE TABLE buy_detail(
id int PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT,
buy_info_id int NOT NULL,
game_id int NOT NULL,
FOREIGN KEY(game_id) REFERENCES game(id)
);

CREATE TABLE bookmark(
id int PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT,
user_id int NOT NULL,
game_id int NOT NULL,
FOREIGN KEY(user_id) REFERENCES user(id),
FOREIGN KEY(game_id) REFERENCES game(id)
);

CREATE TABLE genre(
id int PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT,
name VARCHAR(255) UNIQUE NOT NULL
);

CREATE TABLE review(
id int PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT,
game_id int NOT NULL,
user_id int NOT NULL,
score int(5) NOT NULL,
detail VARCHAR(1000) NOT NULL,
post_date DATE NOT NULL,
FOREIGN KEY(user_id) REFERENCES user(id)
);



